package com.user.arrangement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient

@RestController
public class ArrangementMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrangementMicroServiceApplication.class, args);
	}
	
	@RequestMapping("/arrangement")
	  public String index() {
	    return "Greetings from Spring Boot! This is from Arrangement service";
	  }


}
